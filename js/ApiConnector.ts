/*
 * Copyright (c) 2020. Witt, Nils
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

//return new Promise((resolve, reject) => {});
class ApiConnector {
    url: string;
    dbc: DatabaseConnector;
    token: string;

    constructor(dbc: DatabaseConnector, token: string) {
        this.dbc = dbc;
        this.token = token;
        this.url = "https://saniapi.nils-witt.de";

    }

    updateDatabaseAll() {
        return new Promise(async (resolve, reject) => {
            try {
                console.log("CA")
                let assignments = await this.getAssignments();
                this.dbc.clearAssignments();
                for (const assignmentsKey in assignments) {
                    await this.dbc.saveAssignments(assignments[assignmentsKey]);
                }

                console.log("CC")
                let courses = await this.getCourses();
                this.dbc.clearCourses();
                for (const coursesKey in courses) {
                    await this.dbc.saveCourse(courses[coursesKey]);
                }

                console.log("CL")
                let lessons = await this.getLessons();
                this.dbc.clearLessons();
                for (const lessonsKey in lessons) {
                    await this.dbc.saveLesson(lessons[lessonsKey]);
                }
                console.log("CU")
                let users = await this.getUsers();
                this.dbc.clearUsers();
                for (const usersKey in users) {
                    await this.dbc.saveUser(users[usersKey]);
                }

                let exams = await this.getExams();
                this.dbc.clearExams();
                for (const examKey in exams) {
                    await this.dbc.saveExam(exams[examKey]);
                }
                resolve();
            } catch (e) {
                console.log(e);
                console.log("Error while updating the database");
            }
        });
    }

    updateDatabaseViewOnly() {
        return new Promise(async (resolve, reject) => {
            try {
                console.log("CA")
                let assignments = await this.getAssignments();
                this.dbc.clearAssignments();
                for (const assignmentsKey in assignments) {
                    await this.dbc.saveAssignments(assignments[assignmentsKey]);
                }
                console.log("CU")
                let users = await this.getUsers();
                this.dbc.clearUsers();
                for (const usersKey in users) {
                    console.log(usersKey)
                    await this.dbc.saveUser(users[usersKey]);
                }
                resolve();
            } catch (e) {
                console.log(e);
                console.log("Error while updating the database");
            }
        });
    }

    updateDatabaseAssignments() {
        return new Promise(async (resolve, reject) => {
            try {
                let assignments = await this.getAssignments();
                this.dbc.clearAssignments();
                for (const assignmentsKey in assignments) {
                    await this.dbc.saveAssignments(assignments[assignmentsKey]);
                }
                resolve();
            } catch (e) {
                console.log(e);
                console.log("Error while updating the database");
            }
        });
    }

    getAssignments(): Promise<Assignment[]> {
        return new Promise(async (resolve, reject) => {
            try {
                let response = await fetch(this.url + "/assignments", {
                    method: 'GET',
                    headers: {
                        'Authorization': "Bearer " + this.token
                    },
                });

                if (response.status === 200) {
                    resolve(await response.json());
                }
                if (response.status === 401) {
                    authErr();
                    reject('err');
                }
                if (response.status === 604) {
                    console.log("err");
                    reject('err');
                }
            } catch (e) {
                console.log("e");
                reject("NC")
            }
        });
    }

    setAssignment(assignment: Assignment) {
        return new Promise(async (resolve, reject) => {

            const response = await fetch(this.url + '/assignments', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer " + this.token
                },
                body: JSON.stringify(assignment)
            });
            if (response.status === 200) {
                resolve();
            } else {
                reject();
            }
        });
    }

    deleteAssignment(assignment: Assignment) {
        return new Promise(async (resolve, reject) => {

            const response = await fetch(this.url + '/assignments/id/' + assignment.id, {
                method: 'DELETE',
                headers: {
                    'Authorization': "Bearer " + this.token
                }
            });
            if (response.status === 200) {
                resolve();
            } else {
                reject();
            }
        });
    }

    getCourses(): Promise<Course[]> {
        return new Promise(async (resolve, reject) => {
            try {
                let response = await fetch(this.url + "/timetable/courses", {
                    method: 'GET',
                    headers: {
                        'Authorization': "Bearer " + this.token
                    },
                });

                if (response.status === 200) {
                    resolve(await response.json());
                }
                if (response.status === 401) {
                    authErr();
                    reject('err');
                }
                if (response.status === 604) {
                    console.log("err");
                    reject('err');
                }
            } catch (e) {
                console.log("e");
                reject("NC")
            }
        });
    }

    getLessons(): Promise<Lesson[]> {
        return new Promise(async (resolve, reject) => {
            try {
                let response = await fetch(this.url + "/timetable/lessons", {
                    method: 'GET',
                    headers: {
                        'Authorization': "Bearer " + this.token
                    },
                });

                if (response.status === 200) {
                    resolve(await response.json());
                }
                if (response.status === 401) {
                    authErr();
                    reject('err');
                }
                if (response.status === 604) {
                    console.log("err");
                    reject('err');
                }
            } catch (e) {
                console.log("e");
                reject("NC")
            }
        });
    }

    getUsers(): Promise<User[]> {
        return new Promise(async (resolve, reject) => {
            try {
                let response = await fetch(this.url + "/users", {
                    method: 'GET',
                    headers: {
                        'Authorization': "Bearer " + this.token
                    },
                });

                if (response.status === 200) {
                    resolve(await response.json());
                }
                if (response.status === 401) {
                    authErr();
                    reject('err');
                }
                if (response.status === 604) {
                    console.log("err");
                    reject('err');
                }
            } catch (e) {
                console.log("e");
                reject("NC")
            }
        });
    }

    setUserCourses(user: User): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                console.log(JSON.stringify({courseIds: user.courseIds, examCourseIds: user.examCourseIds}));
                let response = await fetch(this.url + "/users/id/" + user.id + "/courses", {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': "Bearer " + this.token
                    },
                    body: JSON.stringify({courseIds: user.courseIds, examCourseIds: user.examCourseIds})
                });

                if (response.status === 200) {
                    resolve();
                }
                if (response.status === 401) {
                    authErr();
                    reject('err');
                }
                if (response.status === 604) {
                    console.log("err");
                    reject('err');
                }
            } catch (e) {
                console.log("e");
                reject("NC")
            }
        });
    }

    getExams(): Promise<any[]> {
        return new Promise(async (resolve, reject) => {
            try {
                let response = await fetch(this.url + "/timetable/exams", {
                    method: 'GET',
                    headers: {
                        'Authorization': "Bearer " + this.token
                    },
                });

                if (response.status === 200) {
                    resolve(await response.json());
                }
                if (response.status === 401) {
                    authErr();
                    reject('err');
                }
                if (response.status === 604) {
                    console.log("err");
                    reject('err');
                }
            } catch (e) {
                console.log("e");
                reject("NC")
            }
        });
    }

    ldapUserSearch(name): Promise<User[]> {
        return new Promise(async (resolve, reject) => {
            let response = await fetch(this.url + "/ldap/search/name/" + name, {
                method: 'GET',
                headers: {
                    'Authorization': "Bearer " + this.token
                },
            });

            if (response.status === 200) {
                resolve(await response.json());
            }
            if (response.status === 401) {
                authErr();
                reject('err');
            }
            if (response.status === 604) {
                console.log("err");
                reject('err');
            }
        });
    }

    createUser(user) {
        return new Promise(async (resolve, reject) => {
            let response = await fetch(this.url + "/users", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer " + this.token
                },
                body: JSON.stringify(user)
            });

            if (response.status === 200) {
                resolve(await response.json());
            }
            if (response.status === 401) {
                authErr();
                reject('err');
            }
            if (response.status === 604) {
                console.log("err");
                reject('err');
            }
        });
    }

}

async function authErr() {
    console.log("JWT invalid");
    //localStorage.clear();
    //window.location.href = "/views/login.html";
}