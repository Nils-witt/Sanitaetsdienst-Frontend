/*
 * Copyright (c) 2020. Witt, Nils
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

addEventListener('DOMContentLoaded', async () => {
    let databaseConnector = new DatabaseConnector();
    await databaseConnector.initDatabase();
    let saniOverview = new SaniOverview(databaseConnector);
    saniOverview.setTableBody(<HTMLBodyElement>document.getElementById("saniTableBody"));
    saniOverview.setCoursesDiv(<HTMLDivElement>document.getElementById("coursesDiv"));
    saniOverview.setExamsDiv(<HTMLDivElement>document.getElementById("examsDiv"));
    saniOverview.setUserExamsDiv(<HTMLDivElement>document.getElementById("tableBodyUserExams"));
    saniOverview.setUserCoursesDiv(<HTMLDivElement>document.getElementById("tableBodyUserCourses"));
    saniOverview.setTimeTableDiv(<HTMLDivElement>document.getElementById("container"));
    let apiConnector: ApiConnector = new ApiConnector(databaseConnector, window.localStorage.getItem('token'));
    await apiConnector.updateDatabaseAll();

    await saniOverview.createTable();


})


class SaniOverview {
    private userData: User[];
    private tableBody: HTMLBodyElement;
    private databaseConnector: DatabaseConnector;
    private saniOverview: SaniOverview = this;
    private coursesDiv: HTMLDivElement;
    private examsDiv: HTMLDivElement;
    private timeTableDiv: HTMLDivElement;
    private userCoursesDiv: HTMLDivElement;
    private userExamsDiv: HTMLDivElement;

    constructor(databaseConnector: DatabaseConnector) {
        this.databaseConnector = databaseConnector
    }

    setData(userData: User[]) {
        this.userData = userData;
    }

    setTableBody(body: HTMLBodyElement) {
        this.tableBody = body;
    }

    setCoursesDiv(div: HTMLDivElement) {
        this.coursesDiv = div;
    }

    setUserExamsDiv(div: HTMLDivElement) {
        this.userExamsDiv = div;
    }

    setExamsDiv(div: HTMLDivElement) {
        this.examsDiv = div;
    }

    setUserCoursesDiv(div: HTMLDivElement) {
        this.userCoursesDiv = div;
    }

    setTimeTableDiv(div: HTMLDivElement) {
        this.timeTableDiv = div;
    }

    createTable(): Promise<HTMLTableRowElement[]> {
        return new Promise(async (resolve, reject) => {
            this.tableBody.innerHTML = "";
            this.userData = await this.databaseConnector.getUsers();

            this.userData.sort((a: User, b: User): number => {
                return a.username.localeCompare(b.username);
            })

            for (let i = 0; i < this.userData.length; i++) {
                let user: User = this.userData[i];
                if (!(user.grade === null || user.grade === "")) {
                    this.tableBody.append(await this.createRow(user));
                }
            }
            resolve();
        });
    }

    createRow(user: User): Promise<HTMLTableRowElement> {
        return new Promise(async (resolve, reject) => {
            let row = document.createElement('tr');
            let nameTd = document.createElement('td');
            let gradeTd = document.createElement('td');
            let lessonsTd = document.createElement('td');
            let coursesTd = document.createElement('td');
            let timetableTd = document.createElement('td');
            let examsTd = document.createElement('td');
            let settingsTd = document.createElement('td');

            nameTd.innerText = user.lastname + ", " + user.firstname;
            gradeTd.innerText = user.grade;

            lessonsTd.innerText = (await this.databaseConnector.getAssignmentsByUserId(user.id)).length.toString();

            let coursesButton = document.createElement('button');
            coursesButton.innerText = "Öffnen";
            coursesButton.className = "btn btn-primary"
            coursesButton.onclick = () => {
                this.saniOverview.openCoursesDiv(user.courseIds, user.examCourseIds);
            }
            coursesTd.appendChild(coursesButton);

            let timetableButton = document.createElement('button');
            timetableButton.innerText = "Öffnen";
            timetableButton.className = "btn btn-primary"
            timetableButton.onclick = () => {
                this.saniOverview.openTimetableDiv(user.courseIds);
            }
            timetableTd.appendChild(timetableButton);

            let examsButton = document.createElement('button');
            examsButton.innerText = "Öffnen";
            examsButton.className = "btn btn-primary"
            examsButton.onclick = () => {
                this.saniOverview.openExamsDiv(user.examCourseIds);
            }
            examsTd.appendChild(examsButton);

            let settingsButton = document.createElement('button');
            settingsButton.innerText = "Bearbeiten";
            settingsButton.className = "btn btn-primary"
            settingsButton.onclick = () => {
                window.location.href = "/views/editUser.html?user=" + user.id;
            }
            settingsTd.appendChild(settingsButton);

            row.appendChild(nameTd);
            row.appendChild(gradeTd);
            row.appendChild(lessonsTd);
            row.appendChild(coursesTd);
            row.appendChild(timetableTd);
            row.appendChild(examsTd);
            row.appendChild(settingsTd);

            resolve(row);
        });
    }

    async openCoursesDiv(courseIds: number[], examCourseIds: number[]) {
        this.coursesDiv.style.visibility = "visible";

        this.userCoursesDiv.innerHTML = "";
        for (let i = 0; i < courseIds.length; i++) {
            let row = document.createElement('tr');
            let grade = document.createElement('td');
            let subject = document.createElement('td');
            let group = document.createElement('td');

            try {
                let course: Course = await this.databaseConnector.getCourseById(courseIds[i]);

                grade.innerText = course.grade;
                subject.innerText = course.subject;
                group.innerText = course.group;
                if (examCourseIds.includes(courseIds[i])) {
                    group.innerText += " (s.)"
                }
            } catch (e) {
                console.log(e)
                grade.innerText = courseIds[i].toString();
            }

            row.append(grade);
            row.append(subject);
            row.append(group);

            this.userCoursesDiv.append(row);
        }
    }

    async openTimetableDiv(courseIds: number[]) {
        this.timeTableDiv.style.visibility = "visible";

        let userLessons = [];
        for (let i = 0; i < courseIds.length; i++) {
            let course = await this.databaseConnector.getCourseById(courseIds[i]);
            let lessons = await this.databaseConnector.getLessonsByCourse(course);
            for (let j = 0; j < lessons.length; j++) {
                userLessons.push(lessons[j]);
            }
        }
        let timeTable = new TimeTable();
        timeTable.data = new TimeTableData([], userLessons);
        timeTable.prepareData();
        timeTable.generateTable();
    }

    async openExamsDiv(examCourseIds: number[]) {
        console.log(examCourseIds);
        let exams = [];
        for (let i = 0; i < examCourseIds.length; i++) {
            let courseExams = await this.databaseConnector.getExamsByCourse(await this.databaseConnector.getCourseById(examCourseIds[i]))
            for (let j = 0; j < courseExams.length; j++) {
                exams.push(courseExams[j]);
            }
        }
        console.log(exams);

        this.examsDiv.style.visibility = "visible";

        this.userExamsDiv.innerHTML = "";
        for (let i = 0; i < exams.length; i++) {
            let row = document.createElement('tr');
            let courseTd = document.createElement('td');
            let dateTd = document.createElement('td');
            let timeTd = document.createElement('td');

            let exam = exams[i];
            let course = await this.databaseConnector.getCourseById(exam.courseId);

            courseTd.innerText = course.grade + "/" + course.subject + "-" + course.group;
            dateTd.innerText = exam.date;
            timeTd.innerText = exam.from + " -> " + exam.to;

            row.append(courseTd);
            row.append(dateTd);
            row.append(timeTd);

            this.userExamsDiv.append(row);
        }
    }
}


function closeViews() {
    let coursesDiv: HTMLDivElement = <HTMLDivElement>document.getElementById('coursesDiv');
    coursesDiv.style.visibility = "hidden";
    let examDiv: HTMLDivElement = <HTMLDivElement>document.getElementById('examsDiv');
    examDiv.style.visibility = "hidden";
    let daysContainer: HTMLDivElement = <HTMLDivElement>document.getElementById('container');
    daysContainer.style.visibility = "hidden";
}

class TimeTableData {
    lessons: any;
    preparedData;
    private exams: any;

    constructor(exams, lessons) {
        this.exams = exams;
        this.lessons = lessons;
    }
}
