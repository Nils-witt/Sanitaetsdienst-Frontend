/*
 * Copyright (c) 2020. Witt, Nils
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

class TimeTable {
    data: TimeTableData;
    offset: number;
    weekdays: { "1": string; "2": string; "3": string; "4": string; "5": string; "6": string; "7": string };
    timeLessons: number[][];

    constructor() {
        this.offset = 0;
        this.weekdays = {
            1: 'Montag',
            2: 'Dienstag',
            3: 'Mittwoch',
            4: 'Donnerstag',
            5: 'Freitag',
            6: 'Samstag',
            7: 'Sonntag'
        };
        this.timeLessons = [[28200, 30900], [30900, 33600], [34800, 37500], [37800, 40500], [41700, 44400], [44400, 47100]];
    }

    generateTable() {
        if (Object.keys(this.data.preparedData).length > 0) {
            let daysContainer = document.getElementById('daysContainer')
            daysContainer.innerHTML = "";
            for (let day in this.data.preparedData) {
                let htmlDay = this.generateDay(this.data.preparedData[day], day);
                daysContainer.append(htmlDay)
            }

            return daysContainer;
        }
    }

    generateDay(dayObject, dayInt) {
        let day = document.createElement('div');
        day.className = 'col';

        let headline = document.createElement('h5');
        headline.innerText = this.weekdays[dayInt];
        day.append(headline);

        let preKey = 1;
        for (let key in dayObject) {
            if (dayObject.hasOwnProperty(key)) {
                if (parseInt(key) !== preKey) {
                    let diff = parseInt(key) - preKey;
                    for (let i = 0; i < diff; i++) {

                        let main: HTMLDivElement = <HTMLDivElement>document.createElement('div');
                        main.className = 'row';
                        main.style.paddingLeft = '24px';

                        let container = document.createElement('div');
                        container.className = 'col';

                        let text = document.createElement('p');
                        text.innerText = "Freistunde";

                        container.append(text);
                        main.append(container);
                        day.append(main);

                    }
                }
                const lesson = dayObject[key];
                let htmlLesson = this.generateColumn(lesson);
                day.append(htmlLesson);
                preKey = parseInt(key) + 1;
            }
        }

        return day;
    }

    generateColumn(lesson) {
        if (lesson != null) {
            let main = document.createElement('div');
            let container = document.createElement('div');
            let text = document.createElement('p');

            if (lesson.hasOwnProperty("exam")) {
                text.innerText = lesson["exam"]["subject"];
            } else if (lesson.hasOwnProperty("lesson")) {
                text.innerText = lesson;
                text.innerText = lesson["lesson"]["course"]["subject"];
            }

            container.className = 'col';
            container.append(text);
            main.append(container);
            main.className = 'row';
            return main;
        }
    }

    prepareData() {
        let lessons = {};
        let exams = {};

        let data = {};

        this.data.lessons.forEach(lesson => {
            let hour = lesson["lesson"];
            let day = lesson.weekday;

            if (!lessons.hasOwnProperty(day)) {
                lessons[day] = {};
            }
            lessons[day][hour] = lesson;

        });

        Object.keys(lessons).forEach(day => {
            Object.keys(lessons[day]).forEach(lesson => {
                if (!data.hasOwnProperty(day)) {
                    data[day] = {};
                }
                if (!data[day].hasOwnProperty(lesson)) {
                    data[day][lesson] = {}
                }
                data[day][lesson]["lesson"] = lessons[day][lesson];
            })
        });

        this.data.preparedData = data;
        return data;
    }
}