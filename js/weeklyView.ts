/*
 * Copyright (c) 2020. Witt, Nils
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

function convertTimeToSeconds(time) {
    let date = time.split(":");
    return (date[0] * 3600) + (date[1] * 60);
}

class WeeklyView {
    public timeTableBody: HTMLBodyElement;
    private assignments;
    private weekdays: any = {
        1: 'Montag',
        2: 'Dienstag',
        3: 'Mittwoch',
        4: 'Donnerstag',
        5: 'Freitag',
        6: 'Samstag',
        7: 'Sonntag'
    };
    private displayWeek: Date;
    private databaseConnector: DatabaseConnector;
    private weeklyView: WeeklyView = this;

    constructor(databaseConnector: DatabaseConnector) {
        this.databaseConnector = databaseConnector;

        let date = new Date(Date.now());
        date.setDate(date.getDate() - (date.getDay() - 1))
        this.displayWeek = date;
    }

    async loadAssignments() {
        let date = this.displayWeek;
        let dateStr = date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0") + "-" + date.getDate().toString().padStart(2, "0");
        let dbAssignments: Assignment[] = await this.databaseConnector.getAssignemntsByWeek(dateStr);
        this.assignments = {};
        for (let i = 0; i < dbAssignments.length; i++) {
            let assignment = dbAssignments[i];
            let date = new Date(assignment.date);
            let weekday = date.getDay();
            if (!this.assignments.hasOwnProperty(weekday)) {
                this.assignments[weekday] = {};
            }
            if (!this.assignments[weekday].hasOwnProperty(assignment.lesson)) {
                this.assignments[weekday][assignment.lesson] = {};
            }
            this.assignments[weekday][assignment.lesson][assignment.type] = assignment.userId;
        }
    }

    async generateTimetable() {
        let daysContainer = document.createElement('div');
        daysContainer.id = 'daysContainer';
        daysContainer.className = 'row';
        for (let i = 1; i <= 6; i++) {
            if (this.assignments.hasOwnProperty(i)) {
                let htmlDay = await this.generateDay(this.assignments[i], i);
                daysContainer.append(htmlDay)
            } else {
                let htmlDay = await this.generateDay(null, i);
                daysContainer.append(htmlDay)
            }
        }
        this.timeTableBody.innerHTML = "";
        this.timeTableBody.append(daysContainer);
    }

    async generateDay(dayObject, dayInt) {
        let day = document.createElement('div');
        day.className = 'col';

        let headline = document.createElement('h5');
        headline.innerText = this.weekdays[dayInt];
        day.append(headline);

        for (let i = 1; i <= 6; i++) {
            if (dayObject != null && dayObject.hasOwnProperty(i)) {
                let lesson = dayObject[i];
                let htmlLesson = await this.generateColumn(lesson);
                day.append(htmlLesson);
            } else {
                day.append(await this.generateColumn(null));
            }
        }
        return day;
    }

    async generateColumn(saniObject: { 1: string, 2: string }) {
        let main = document.createElement('div');
        let container = document.createElement('div');
        let text = document.createElement('p');
        let text2 = document.createElement('p');
        if (saniObject != null) {
            try {
                let name1;
                let name2;

                try {
                    let user: User = await this.databaseConnector.getUserById(parseInt(saniObject["1"]));
                    name1 = user.lastname + ", " + user.firstname;
                } catch (e) {
                }

                try {
                    let user: User = await this.databaseConnector.getUserById(parseInt(saniObject["2"]));
                    name2 = user.lastname + ", " + user.firstname;
                } catch (e) {
                }

                if (name1 == null) {
                    name1 = name2 + " (2.)";
                    name2 = undefined;
                }
                text.innerText = name1;

                if (name2 !== undefined) {
                    text2.innerText = " (" + name2 + ")";
                } else {
                    text2.innerText = " (Keine Zuordnung)";
                }
            } catch (e) {
                console.error(e);
            }
        } else {
            text.innerText = "Keine Zuordnung"
            text2.innerText = " (Keine Zuordnung)";
        }

        container.className = 'col tableColumn';
        //container.onclick = function() {openDetailView(lesson)};
        //container.style.cursor = "pointer";
        container.style.minWidth = "200px";
        container.append(text);
        container.append(text2);
        main.append(container);
        main.className = 'row';
        return main;

    }

    updatePagination() {
        let date = new Date(this.displayWeek);

        document.getElementById("currentWeek").innerText = date.getDate().toString().padStart(2, '0') + "." + (date.getMonth() + 1).toString().padStart(2, "0");
        date.setDate(date.getDate() - 7);
        let previousWeek = document.getElementById("previousWeek");
        previousWeek.innerText = date.getDate().toString().padStart(2, '0') + "." + (date.getMonth() + 1).toString().padStart(2, "0");
        previousWeek.onclick = () => {
            updateSelectedWeekView(-1, this.weeklyView)
        }
        date.setDate(date.getDate() + 14);
        let nextWeek = document.getElementById("nextWeek");
        nextWeek.innerText = date.getDate().toString().padStart(2, '0') + "." + (date.getMonth() + 1).toString().padStart(2, "0");
        nextWeek.onclick = () => {
            updateSelectedWeekView(1, this.weeklyView)
        }
    }

    async updateSelectedWeek(offset) {
        let date = this.displayWeek;
        date.setDate(date.getDate() + (offset * 7));
        this.updatePagination();
        await this.loadAssignments();
        await this.generateTimetable();
    }
}

function updateSelectedWeekView(offset: number, weeklyView: WeeklyView) {
    weeklyView.updateSelectedWeek(offset);
}
