/*
 * Copyright (c) 2020. Witt, Nils
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

addEventListener('DOMContentLoaded', async () => {
    if (!(window.localStorage.getItem('token') === "" || window.localStorage.getItem('token') == null)) {
        window.location.href = "/views/weeklyView.html";
    }

    let loginButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById("loginButton");
    loginButton.disabled = false;

});


function login() {
    let loginButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById("loginButton");
    loginButton.disabled = true;
    let usernameField: HTMLInputElement = <HTMLInputElement>document.getElementById("username")
    let passwordField: HTMLInputElement = <HTMLInputElement>document.getElementById("password")
    let loginErrorText: HTMLInputElement = <HTMLInputElement>document.getElementById("loginError")
    loginErrorText.style.visibility = 'hidden';
    requestTokenFromApi(usernameField.value, passwordField.value);

}

function requestTokenFromApi(username: string, password: string) {
    return new Promise(async (resolve, reject) => {

        const response = await fetch('https://saniapi.nils-witt.de/user/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({username: username, password: password})
        });
        if (response.status === 200) {
            console.log("LS");
            let data = await response.json();
            window.localStorage.setItem("token", data.token);
            window.location.href = "/views/weeklyView.html";
        } else {
            console.log("LF")
            let loginButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById("loginButton");
            loginButton.disabled = false;
            let loginErrorText: HTMLInputElement = <HTMLInputElement>document.getElementById("loginError")
            loginErrorText.style.visibility = 'visible';
        }
        resolve();
    });
}