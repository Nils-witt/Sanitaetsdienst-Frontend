/*
 * Copyright (c) 2020. Witt, Nils
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

addEventListener('DOMContentLoaded', async () => {
    let urlParams = getUrlParams();
    let dbc = new DatabaseConnector();
    let apc = new ApiConnector(dbc, window.localStorage.getItem("token"));
    await dbc.initDatabase();
    let user = await dbc.getUserById(urlParams.user);
    document.getElementById('nameField').innerText = user.displayname;

    let coursesBody = document.getElementById('coursesTbody');
    for (let i = 0; i < user.courseIds.length; i++) {
        try {
            let course = await dbc.getCourseById(user.courseIds[i]);
            let row = document.createElement('tr');
            let grade = document.createElement('td');
            let subject = document.createElement('td');
            let group = document.createElement('td');
            let exams = document.createElement('td');
            let actions = document.createElement('td');

            let deleteButton = document.createElement('button');
            deleteButton.innerText = 'Löschen';
            deleteButton.className = "btn btn-danger";

            deleteButton.onclick = () => {
                let index: number = user.courseIds.indexOf(course.id);
                let indexExam: number = user.examCourseIds.indexOf(course.id);
                if (index >= 0) {
                    user.courseIds.splice(index, 1);
                }
                if (indexExam >= 0) {
                    user.examCourseIds.splice(indexExam, 1);
                }
                dbc.updateUserRecord(user);
                apc.setUserCourses(user);
                window.location.reload();
            }

            let examsToggleButton = document.createElement('button');
            examsToggleButton.innerText = 'Klausuren AN/Aus';
            examsToggleButton.className = "btn btn-primary";

            examsToggleButton.onclick = () => {
                if (user.examCourseIds.includes(course.id)) {
                    let index: number = user.examCourseIds.indexOf(course.id);
                    if (index >= 0) {
                        user.examCourseIds.splice(index, 1);
                    }
                } else {
                    user.examCourseIds.push(course.id);
                }

                dbc.updateUserRecord(user);
                apc.setUserCourses(user);
                window.location.reload();
            }

            actions.append(deleteButton);
            actions.append(examsToggleButton);

            grade.innerText = course.grade;
            subject.innerText = course.subject;
            group.innerText = course.group;

            if (user.examCourseIds.includes(course.id)) {
                exams.innerText = "Ja";
            } else {
                exams.innerText = "Nein";
            }

            row.append(grade);
            row.append(subject);
            row.append(group);
            row.append(exams);
            row.append(actions);

            coursesBody.append(row);
        } catch (e) {
            console.log(e);
        }
    }

    document.getElementById('gradeSelect').onchange = async (evnt) => {
        let target = <HTMLSelectElement>evnt.target;
        console.log(target.options[target.options.selectedIndex].innerText)
        let courses = await dbc.getCourseByGrade(target.options[target.options.selectedIndex].innerText);
        let subjects = [];
        console.log(courses)
        for (let i = 0; i < courses.length; i++) {
            if (!subjects.includes(courses[i].subject)) {
                subjects.push(courses[i].subject);
            }
        }
        let subjectContainer = document.getElementById('subjectSelect');
        subjectContainer.innerHTML = "";
        let option = document.createElement('option');
        option.innerHTML = "Auswählen";
        subjectContainer.append(option);

        subjects.sort();

        for (let i = 0; i < subjects.length; i++) {
            let option = document.createElement('option');
            option.innerHTML = subjects[i];
            subjectContainer.append(option);
        }
    }

    document.getElementById('subjectSelect').onchange = async (evnt) => {
        let target = <HTMLSelectElement>evnt.target;
        console.log(target.options[target.options.selectedIndex].innerText)

        let gradeSelect = <HTMLSelectElement>document.getElementById('gradeSelect');
        let grade = gradeSelect.options[gradeSelect.options.selectedIndex].innerText
        let courses = await dbc.getCourseByGradeSubject(grade, target.options[target.options.selectedIndex].innerText);
        let groups = [];

        for (let i = 0; i < courses.length; i++) {
            if (!groups.includes(courses[i].group)) {
                groups.push(courses[i].group);
            }
        }
        let subjectContainer = document.getElementById('groupSelect');
        subjectContainer.innerHTML = "";

        let option = document.createElement('option');
        option.innerHTML = "Auswählen";
        subjectContainer.append(option);

        groups.sort();
        for (let i = 0; i < groups.length; i++) {
            let option = document.createElement('option');
            option.innerHTML = groups[i];
            subjectContainer.append(option);
        }
    }

    document.getElementById('addCourseButton').onclick = async () => {
        let gradeSelect = <HTMLSelectElement>document.getElementById('gradeSelect');
        let grade = gradeSelect.options[gradeSelect.options.selectedIndex].innerText
        let subjectSelect = <HTMLSelectElement>document.getElementById('subjectSelect');
        let subject = subjectSelect.options[subjectSelect.options.selectedIndex].innerText
        let groupSelect = <HTMLSelectElement>document.getElementById('groupSelect');
        let group = groupSelect.options[groupSelect.options.selectedIndex].innerText

        let course = await dbc.getCourseByGradeSubjectGroup(grade, subject, group);
        console.log(course)
        user.courseIds.push(course.id);
        await dbc.updateUserRecord(user);
        await apc.setUserCourses(user);
        window.location.reload();
    }

});

function getUrlParams(): { user: number } {
    let urlParamStr = window.location.search.substr(1);
    let urlParams = urlParamStr.split("&");
    let urlParamObject = {user: 0};
    for (let i = 0; i < urlParams.length; i++) {
        let keyValue = urlParams[i].split("=");
        urlParamObject[keyValue[0]] = parseInt(keyValue[1]);
    }
    return urlParamObject;
}

