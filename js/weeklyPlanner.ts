/*
 * Copyright (c) 2020. Witt, Nils
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

class WeeklyPlanner {
    public weeklyPlanner: WeeklyPlanner;
    public databaseConnector: DatabaseConnector;
    public saniTBody: HTMLBodyElement;
    public timeTableBody: HTMLBodyElement;
    private assignments;
    private exams;
    private weekdays: any = {
        1: 'Montag',
        2: 'Dienstag',
        3: 'Mittwoch',
        4: 'Donnerstag',
        5: 'Freitag',
        6: 'Samstag',
        7: 'Sonntag'
    };
    private displayWeek: Date;
    private users;
    private selector: [number, number, number, HTMLParagraphElement];
    private apiConnector: ApiConnector;


    constructor(apiConnector: ApiConnector) {
        this.apiConnector = apiConnector;
        let date = new Date(Date.now());
        date.setDate(date.getDate() - (date.getDay() - 1))
        this.displayWeek = date;
    }

    async setUsers(users) {
        this.users = {};
        for (let i = 0; i < users.length; i++) {
            let user = users[i];

            user.timeTable = {};
            for (let j = 1; j <= 6; j++) {
                user.timeTable[j] = {};
                for (let k = 1; k <= 6; k++) {
                    user.timeTable[j][k] = 0;
                }
            }
            for (let j = 0; j < user.courseIds.length; j++) {
                let lessons: Lesson[] = await this.databaseConnector.getLessonsByCourseId(user.courseIds[j])
                for (let k = 0; k < lessons.length; k++) {
                    let lesson = lessons[k];
                    user.timeTable[lesson.weekday][lesson.lesson] = 1;
                }
            }
            this.users[user.id] = user;
        }
    }

    async loadAssignments() {

        let date = this.displayWeek;
        let dateStr = date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0") + "-" + date.getDate().toString().padStart(2, "0");
        let dbAssignments: Assignment[] = await this.databaseConnector.getAssignemntsByWeek(dateStr);
        this.assignments = {};
        for (let i = 0; i < dbAssignments.length; i++) {
            let assignment = dbAssignments[i];
            let date = new Date(assignment.date);
            let weekday = date.getDay();
            if (!this.assignments.hasOwnProperty(weekday)) {
                this.assignments[weekday] = {};

            }
            if (!this.assignments[weekday].hasOwnProperty(assignment.lesson)) {
                this.assignments[weekday][assignment.lesson] = {};

            }

            this.assignments[weekday][assignment.lesson][assignment.type] = assignment.userId;
        }
    }

    async loadExams() {

        let date = this.displayWeek;
        let dateStr = date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0") + "-" + date.getDate().toString().padStart(2, "0");
        let dbExams: Exam[] = await this.databaseConnector.getExamsByWeek(dateStr);
        this.exams = {};
        for (let i = 0; i < dbExams.length; i++) {
            let exam = dbExams[i];
            let date = new Date(exam.date);
            let weekday = date.getDay();
            if (!this.exams.hasOwnProperty(weekday)) {
                this.exams[weekday] = [];
            }
            this.exams[weekday].push(exam.courseId);
        }
        console.log(this.exams);
    }

    generateTimetable() {
        let daysContainer = document.createElement('div');
        daysContainer.id = 'daysContainer';
        daysContainer.className = 'row';

        for (let i = 1; i <= 6; i++) {
            if (this.assignments.hasOwnProperty(i)) {
                let htmlDay = this.generateDay(this.assignments[i], i);
                daysContainer.append(htmlDay);
            } else {
                let htmlDay = this.generateDay(null, i);
                daysContainer.append(htmlDay);
            }
        }
        this.timeTableBody.innerHTML = "";
        this.timeTableBody.append(daysContainer);
    }

    generateDay(dayObject, dayInt) {
        let day = document.createElement('div');
        day.className = 'col';

        let headline = document.createElement('h5');
        headline.innerText = this.weekdays[dayInt];

        let first = document.createElement('p');
        first.innerText = "1. Sani";
        first.onclick = () => {
            setSelector([dayInt, 0, 1, null], this.weeklyPlanner);
        };
        let second = document.createElement('p');
        second.innerText = "2. Sani";
        second.onclick = () => {
            setSelector([dayInt, 0, 2, null], this.weeklyPlanner);
        };
        second.style.cursor = 'pointer';
        first.style.cursor = 'pointer';

        day.append(headline);
        day.append(first);
        day.append(second);

        for (let i = 1; i <= 6; i++) {
            if (dayObject != null && dayObject.hasOwnProperty(i)) {
                let lesson = dayObject[i];
                let htmlLesson = this.generateColumn(lesson, dayInt, i);
                day.append(htmlLesson);
            } else {
                day.append(this.generateColumn(null, dayInt, i));
            }
        }
        return day;
    }

    generateColumn(saniObject: { 1: number, 2: number }, dayInt: number, lessonInt: number) {
        let main = document.createElement('div');
        let container = document.createElement('div');
        let text = document.createElement('p');
        let text2 = document.createElement('p');

        if (saniObject != null) {
            try {
                let name1
                let name2;

                try {
                    name1 = this.users[saniObject["1"]].lastname + ", " + this.users[saniObject["1"]].firstname;
                } catch (e) {
                }

                try {
                    name2 = this.users[saniObject["2"]].lastname + ", " + this.users[saniObject["2"]].firstname;
                } catch (e) {

                }

                if (name1 !== undefined) {
                    text.innerText = name1;
                } else {
                    text.innerText = " Zuordnen";
                }

                if (name2 !== undefined) {
                    text2.innerText = " (" + name2 + ")";
                } else {
                    text2.innerText = " (Zuordnen)";
                }
            } catch (e) {
                console.error(e);
            }

        } else {
            text.innerText = "Zuordnen"
            text2.innerText = " (Zuordnen)";
        }

        container.className = 'col tableColumn';
        text.onclick = () => {
            setSelector([dayInt, lessonInt, 1, text], this.weeklyPlanner)
        };
        text2.onclick = () => {
            setSelector([dayInt, lessonInt, 2, text2], this.weeklyPlanner)
        };
        container.style.cursor = "pointer";
        container.style.minWidth = "200px";
        container.append(text);
        container.append(text2);
        main.append(container);
        main.className = 'row';

        return main;
    }

    async generateUserTableBySelector() {
        let groupedByLessons = {};
        for (const usersKey in this.users) {
            if (this.users.hasOwnProperty(usersKey)) {
                let user = this.users[usersKey];
                if (!groupedByLessons.hasOwnProperty(user.lessonsUsed)) {
                    groupedByLessons[user.lessonsUsed] = [];
                }
                groupedByLessons[user.lessonsUsed].push(user);
            }
        }

        for (const groupedByLessonsKey in groupedByLessons) {
            if (groupedByLessons.hasOwnProperty(groupedByLessonsKey)) {
                groupedByLessons[groupedByLessonsKey].sort(compareUsers);
            }
        }

        let rows: HTMLTableRowElement[] = [];
        this.saniTBody.innerHTML = "";
        let availableUsers: User[] = [];
        for (const groupedByLessonsKey in groupedByLessons) {
            if (groupedByLessons.hasOwnProperty(groupedByLessonsKey)) {
                let users = groupedByLessons[groupedByLessonsKey];
                for (let i = 0; i < users.length; i++) {
                    let user = users[i];

                    let date = new Date(this.displayWeek);
                    date.setDate(date.getDate() + this.selector[0] - 1);
                    let dateString = date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0") + "-" + date.getDate().toString().padStart(2, "0");
                    if (await this.userIsAvailable(user, [this.selector[0], this.selector[1], this.selector[2]], dateString)) {
                        user.assignedLessons = (await this.databaseConnector.getAssignmentsByUserId(user.id)).length;
                        availableUsers.push(user);
                    }
                }
            }
        }

        availableUsers.sort((a: User, b: User): number => {
            if (a.assignedLessons === b.assignedLessons) {
                return a.username.localeCompare(b.username);
            } else {
                if (a.assignedLessons > b.assignedLessons) {
                    return 1;
                } else {
                    return -1;
                }
            }
        })

        for (let i = 0; i < availableUsers.length; i++) {
            let user: User = availableUsers[i];

            let row = await this.generateUserRow(user)
            rows.push(row);
            this.saniTBody.append(row);
        }

        return rows;
    }

    userIsAvailable(user, selector: [number, number, number], date: string) {
        return new Promise(async (resolve, reject) => {
            console.log("Eids: " + user.displayname);
            console.log(user.examCourseIds);
            console.log(this.exams[selector[0]]);
            let exam = false;
            for (let i = 0; i < user.examCourseIds.length; i++) {
                if (this.exams.hasOwnProperty(selector[0]) && this.exams[selector[0]].includes(user.examCourseIds[i])) {
                    exam = true;
                    console.log("exam: " + user.displayname)
                }
            }
            if (exam === true) {
                resolve(false);
            }
            if (user.hasOwnProperty('timeTable')) {
                let timeTable = user.timeTable;
                if (timeTable.hasOwnProperty(selector[0])) {
                    let day = timeTable[selector[0]];

                    if (selector[1] === 0) {
                        let available = true;
                        for (let i = 1; i <= 6; i++) {
                            if (day.hasOwnProperty(i) && day[i] !== 1) {
                                available = false;
                            }
                        }
                        resolve(available);
                    } else if (day.hasOwnProperty(selector[1])) {
                        let lesson = day[selector[1]];
                        if (lesson === 1) {
                            let isAssignedToOtherSani: boolean = false;
                            let assignments = await this.databaseConnector.getAssignmentsByDateLesson(date, selector[1]);
                            for (let i = 0; i < assignments.length; i++) {
                                let assignment = assignments[i];
                                if (assignment.type !== selector[2] && assignment.userId === user.id) {
                                    isAssignedToOtherSani = true;
                                }
                            }
                            resolve(!isAssignedToOtherSani);
                        }
                    }
                }
            }
            resolve(false);
        });
    }

    async generateUserRow(user): Promise<HTMLTableRowElement> {
        let row = document.createElement('tr');
        let nameField = document.createElement('td');
        let lessonsField = document.createElement('td');
        nameField.innerText = user.lastname + ", " + user.firstname;
        let assignmetsByUser: Assignment[] = await this.databaseConnector.getAssignmentsByUserId(user.id)
        lessonsField.innerText = assignmetsByUser.length.toString();
        row.append(nameField);
        row.append(lessonsField);
        row.onclick = () => {
            setUserOnSelector(user.id, this.weeklyPlanner);
        }
        return row;
    }

    async setSelector(selectorPosition: [number, number, number, HTMLParagraphElement]) {
        try {
            if (this.selector !== undefined && this.selector[3] !== null) {
                if (this.selector[2] === 1) {
                    this.selector[3].innerText = "Zuordnen";
                } else {
                    this.selector[3].innerText = "(Zuordnen)";
                }
            }

            this.selector = selectorPosition;
            if (this.selector[3] !== null) {
                selectorPosition[3].innerText = "Ausgewählt";
            }

            let date = new Date(this.displayWeek);
            date.setDate(date.getDate() + (this.selector[0] - 1))
            let dateStr = date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0") + "-" + date.getDate().toString().padStart(2, "0");

            if (this.selector[1] === 0) {
                for (let i = 1; i <= 6; i++) {
                    try {
                        await this.apiConnector.deleteAssignment(await this.databaseConnector.getAssignmentsByDateLessonType(dateStr, i, selectorPosition[2]));
                        await this.databaseConnector.deleteAssignment(dateStr, i, selectorPosition[2]);
                    } catch (e) {

                    }
                }
            } else {
                try {
                    await this.apiConnector.deleteAssignment(await this.databaseConnector.getAssignmentsByDateLessonType(dateStr, selectorPosition[1], selectorPosition[2]));
                    await this.databaseConnector.deleteAssignment(dateStr, selectorPosition[1], selectorPosition[2]);
                } catch (e) {
                    //console.log(e);
                }
            }

            let selectorText = document.getElementById("saniSelectorInfo");
            if (selectorPosition[1] === 0) {
                selectorText.innerText = this.weekdays[selectorPosition[0]] + ", " + selectorPosition[2] + ". Sani";
            } else {
                selectorText.innerText = this.weekdays[selectorPosition[0]] + ", " + selectorPosition[1] + ". Stunde, " + selectorPosition[2] + ". Sani";
            }

            await this.generateUserTableBySelector();
        } catch (e) {
            console.log("e" + e);
        }
    }

    async setUserOnSelector(userId) {
        console.log("Setting user:" + userId + " On: " + this.selector.toString())
        let userMatching;

        for (const usersKey in this.users) {
            if (this.users.hasOwnProperty(usersKey)) {
                let user = this.users[usersKey];
                if (user.id === userId) {
                    userMatching = user;
                }
            }
        }
        if (userMatching != null) {
            if (this.selector[1] !== 0) {

                let assignment = new Assignment();
                assignment.lesson = this.selector[1];
                assignment.type = this.selector[2];
                assignment.userId = userId;

                let date = new Date(this.displayWeek);
                date.setDate(date.getDate() + (this.selector[0] - 1))
                assignment.date = date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0") + "-" + date.getDate().toString().padStart(2, "0");
                this.selector[3].innerHTML = userMatching.lastname + ", " + userMatching.firstname;

                await this.apiConnector.setAssignment(assignment);
                await this.databaseConnector.updateAssignment(assignment);
                await this.apiConnector.updateDatabaseAssignments();
            } else {
                let date = new Date(this.displayWeek);
                date.setDate(date.getDate() + (this.selector[0] - 1))
                let dateStr = date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0") + "-" + date.getDate().toString().padStart(2, "0");

                for (let i = 1; i <= 6; i++) {
                    let assignment = new Assignment();
                    assignment.lesson = i;
                    assignment.type = this.selector[2];
                    assignment.userId = userId;
                    assignment.date = dateStr;

                    await this.apiConnector.setAssignment(assignment);
                    await this.databaseConnector.updateAssignment(assignment);
                }
                await this.apiConnector.updateDatabaseAssignments();
                await this.loadAssignments();
                await this.generateTimetable();
            }
            this.clearSelector();
        }
    }

    clearSelector() {
        this.selector = undefined;
        document.getElementById("saniSelectorInfo").innerText = "Keine Auswahl";
        this.saniTBody.innerHTML = "";
    }

    updatePagination() {
        let date = new Date(this.displayWeek);

        document.getElementById("currentWeek").innerText = date.getDate().toString().padStart(2, '0') + "." + (date.getMonth() + 1).toString().padStart(2, "0");
        date.setDate(date.getDate() - 7);
        let previousWeek = document.getElementById("previousWeek");
        previousWeek.innerText = date.getDate().toString().padStart(2, '0') + "." + (date.getMonth() + 1).toString().padStart(2, "0");
        previousWeek.onclick = () => {
            updateSelectedWeekPlanner(-1, this.weeklyPlanner)
        }
        date.setDate(date.getDate() + 14);
        let nextWeek = document.getElementById("nextWeek");
        nextWeek.innerText = date.getDate().toString().padStart(2, '0') + "." + (date.getMonth() + 1).toString().padStart(2, "0");
        nextWeek.onclick = () => {
            updateSelectedWeekPlanner(1, this.weeklyPlanner)
        }
    }

    async updateSelectedWeek(offset) {
        let date = this.displayWeek;
        date.setDate(date.getDate() + (offset * 7));
        this.updatePagination();
        let dateStr = date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0") + "-" + date.getDate().toString().padStart(2, '0');
        await this.loadAssignments();
        await this.loadExams();
        this.generateTimetable();
    }
}

function setSelector(selector: [number, number, number, HTMLParagraphElement], weeklyPlanner: WeeklyPlanner) {
    weeklyPlanner.setSelector(selector);

}

function setUserOnSelector(userId, weeklyPlanner) {
    weeklyPlanner.setUserOnSelector(userId);
}

function updateSelectedWeekPlanner(offset: number, weeklyPlanner: WeeklyPlanner) {
    weeklyPlanner.updateSelectedWeek(offset);
}

function compareUsers(a, b) {
    if (a.lastname < b.lastname) {
        return -1;
    }
    if (a.lastname > b.lastname) {
        return 1;
    }
    return 0;
}