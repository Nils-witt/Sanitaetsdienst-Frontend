/*
 * Copyright (c) 2020. Witt, Nils
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

let apiUrl = "https://saniapi.nils-witt.de";
let apc = new ApiConnector(new DatabaseConnector(), window.localStorage.getItem("token"));
addEventListener('DOMContentLoaded', async () => {

    document.getElementById('searchButton').onclick = (event) => {
        (<HTMLButtonElement>document.getElementById('searchButton')).disabled = true;
        let searchElement = <HTMLInputElement>document.getElementById('searchInput');
        let searchValue = searchElement.value;
        search(searchValue);
    };

    document.getElementById('createButton').onclick = createUser;
});

function closeAllViews() {
    document.getElementById('searchResults').style.visibility = 'hidden';
}

async function search(name) {
    let resultsTable = <HTMLBodyElement>document.getElementById('tableBodySearchResults');
    resultsTable.innerHTML = "";

    let users: User[] = await apc.ldapUserSearch(name);

    for (let i = 0; i < users.length; i++) {
        let user: User = users[i];

        let dnParts = user.dn.split(",");
        if (dnParts.length >= 2) {
            let gradeStr = dnParts[1].substr(3);
            if (gradeStr.startsWith("Q2") || gradeStr.startsWith("Q1") || gradeStr.startsWith("EF")) {
                user.grade = gradeStr.substr(0, 2);
            }
        }

        let row = document.createElement('tr');
        let nameTd = document.createElement('td');
        let userNameTd = document.createElement('td');

        row.onclick = () => {
            selectUser(user);
        };

        nameTd.innerText = user.displayname;
        userNameTd.innerText = user.username;

        row.append(nameTd);
        row.append(userNameTd);

        resultsTable.append(row);
    }

    document.getElementById('searchResults').style.visibility = 'visible';
    (<HTMLButtonElement>document.getElementById('searchButton')).disabled = false;
}

function selectUser(user: User) {
    let usernameInput = <HTMLInputElement>document.getElementById('usernameInput');
    let gradeInput = <HTMLInputElement>document.getElementById('gradeInput');
    let firstnameInput = <HTMLInputElement>document.getElementById('firstnameInput');
    let lastnameInput = <HTMLInputElement>document.getElementById('lastnameInput');

    usernameInput.value = user.username;
    gradeInput.value = user.grade;
    firstnameInput.value = user.firstname;
    lastnameInput.value = user.lastname;
    (<HTMLButtonElement>document.getElementById('createButton')).disabled = false;
    document.getElementById('searchResults').style.visibility = 'hidden';
}

function createUser() {
    let createButton = <HTMLButtonElement>document.getElementById('createButton');
    let usernameInput = <HTMLInputElement>document.getElementById('usernameInput');
    let gradeInput = <HTMLInputElement>document.getElementById('gradeInput');
    let firstnameInput = <HTMLInputElement>document.getElementById('firstnameInput');
    let lastnameInput = <HTMLInputElement>document.getElementById('lastnameInput');

    createButton.disabled = true;
    let user = {
        "firstname": firstnameInput.value,
        "lastname": lastnameInput.value,
        "username": usernameInput.value,
        "grade": gradeInput.value,
        "type": 1
    };
    apc.createUser(user).then(r => {
        window.location.href = '/views/saniOverview.html';
    }).catch(e => {
        createButton.disabled = false;
    });
}