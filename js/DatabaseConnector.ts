/*
 * Copyright (c) 2020. Witt, Nils
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

class DatabaseConnector {
    db = undefined;

    initDatabase(): Promise<boolean> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise(async (resolve, reject) => {
            if (databaseConnector.db !== undefined) {
                resolve(false);
                return;
            }
            let request = indexedDB.open('saniDienst', 14);

            request.onupgradeneeded = () => {
                databaseConnector.db = request.result;


                if (databaseConnector.db.objectStoreNames.contains('lessons')) {
                    databaseConnector.db.deleteObjectStore('lessons')
                }
                let store = databaseConnector.db.createObjectStore('lessons', {
                    keyPath: 'key',
                    autoIncrement: true
                });
                store.createIndex("weekday", "weekday", {unique: false});
                store.createIndex("lesson", "lesson", {unique: false});
                store.createIndex("lessonDay", ["weekday", "lesson"], {unique: false});
                store.createIndex("courseId", "courseId", {unique: false});


                if (databaseConnector.db.objectStoreNames.contains('exams')) {
                    databaseConnector.db.deleteObjectStore('exams')
                }
                store = databaseConnector.db.createObjectStore('exams', {
                    keyPath: 'key',
                    autoIncrement: true
                });
                store.createIndex("date", "date", {unique: false});
                store.createIndex("dateSec", "dateSec", {unique: false});
                store.createIndex("courseId", "courseId", {unique: false});


                if (databaseConnector.db.objectStoreNames.contains('users')) {
                    databaseConnector.db.deleteObjectStore('users')
                }
                store = databaseConnector.db.createObjectStore('users', {
                    keyPath: 'key',
                    autoIncrement: true
                });
                store.createIndex("id", "id", {unique: true});


                if (databaseConnector.db.objectStoreNames.contains('courses')) {
                    databaseConnector.db.deleteObjectStore('courses')
                }
                store = databaseConnector.db.createObjectStore('courses', {
                    keyPath: 'key',
                    autoIncrement: true
                });
                store.createIndex("grade", "grade", {unique: false});
                store.createIndex("group", "group", {unique: false});
                store.createIndex("subject", "subject", {unique: false});
                store.createIndex("id", "id", {unique: true});

                store.createIndex("gradeSubject", ['grade', 'subject'], {unique: false});
                store.createIndex("gradeSubjectGroup", ['grade', 'subject', 'group'], {unique: true});


                if (databaseConnector.db.objectStoreNames.contains('saniAssignments')) {
                    databaseConnector.db.deleteObjectStore('saniAssignments')
                }
                store = databaseConnector.db.createObjectStore('saniAssignments', {
                    keyPath: 'key',
                    autoIncrement: true
                });

                store.createIndex("date", "date", {unique: false});
                store.createIndex("lesson", "lesson", {unique: false});
                store.createIndex("userId", "userId", {unique: false});
                store.createIndex("dateSec", "dateSec", {unique: false});
                store.createIndex("assignmentId", ['date', 'lesson', 'type'], {unique: true});
                store.createIndex("dateLesson", ['date', 'lesson'], {unique: false});

            };

            request.onerror = (err) => {
                console.log(err);
                reject('err');
            };

            request.onsuccess = () => {
                databaseConnector.db = request.result;
                resolve(true);
            }
        });
    }

    saveUser(user: User): Promise<void> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let transaction = databaseConnector.db.transaction("users", "readwrite");

            transaction.onerror = (event) => {
                console.log(event)
            };

            let objectStore = transaction.objectStore("users");

            let request = objectStore.add(user);
            request.onsuccess = (event) => {
                resolve();
            };
        });

    }

    getUsers(): Promise<User[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let objectStore = databaseConnector.db.transaction("users").objectStore("users");
            let users: User[] = [];

            objectStore.openCursor().onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    users.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(users)
                }
            };
        });
    }

    getUserById(userId: number): Promise<User> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise(function (resolve, reject) {
            let objectStore = databaseConnector.db.transaction("users").objectStore("users");

            let range = IDBKeyRange.only(userId);

            let index = objectStore.index("id");
            let found = false;
            index.openCursor(range).onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    resolve(cursor.value);
                    found = true;
                    cursor.continue();
                } else if (!found) {
                    reject("User Not found");
                }
            };
        });
    }

    updateUserRecord(user: User): Promise<User> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise(function (resolve, reject) {
            let objectStore = databaseConnector.db.transaction("users", "readwrite").objectStore("users");

            let request = objectStore.put(user);

            request.onsuccess = () => {
                resolve();
            }
            request.onerror = (e) => {
                reject(e);
            }
        });
    }

    clearUsers(): void {
        this.db.transaction("users", "readwrite").objectStore("users").clear();
    }

    saveCourse(course: Course): Promise<void> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let transaction = databaseConnector.db.transaction("courses", "readwrite");

            transaction.onerror = (event) => {
                console.log(event)
            };

            let objectStore = transaction.objectStore("courses");

            let request = objectStore.add(course);
            request.onsuccess = (event) => {
                resolve();
            };
        });

    }

    getCourses(): Promise<Course[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let objectStore = databaseConnector.db.transaction("courses").objectStore("courses");
            let courses: Course[] = [];

            objectStore.openCursor().onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    courses.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(courses)
                }
            };
        });
    }

    getCourseById(courseId: number): Promise<Course> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise(function (resolve, reject) {
            let objectStore = databaseConnector.db.transaction("courses").objectStore("courses");

            let range = IDBKeyRange.only(courseId);

            let index = objectStore.index("id");
            let found = false;
            index.openCursor(range).onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    resolve(cursor.value);
                    found = true;
                    cursor.continue();
                } else if (!found) {
                    reject("Course Not found");
                }
            };
        });
    }

    getCourseByGrade(grade: string): Promise<Course[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise(function (resolve, reject) {
            let objectStore = databaseConnector.db.transaction("courses").objectStore("courses");

            let range = IDBKeyRange.only(grade);

            let index = objectStore.index("grade");
            let courses = []
            index.openCursor(range).onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    courses.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(courses);
                }
            };
        });
    }

    getCourseByGradeSubject(grade: string, subject: string): Promise<Course[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise(function (resolve, reject) {
            let objectStore = databaseConnector.db.transaction("courses").objectStore("courses");

            let range = IDBKeyRange.only([grade, subject]);

            let index = objectStore.index("gradeSubject");
            let courses = []
            index.openCursor(range).onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    courses.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(courses);
                }
            };
        });
    }

    getCourseByGradeSubjectGroup(grade: string, subject: string, group: string): Promise<Course> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise(function (resolve, reject) {
            let objectStore = databaseConnector.db.transaction("courses").objectStore("courses");

            let range = IDBKeyRange.only([grade, subject, group]);

            let index = objectStore.index("gradeSubjectGroup");
            let courses = []
            index.openCursor(range).onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    resolve(cursor.value);
                } else {
                    reject(courses);
                }
            };
        });
    }

    clearCourses(): void {
        this.db.transaction("courses", "readwrite").objectStore("courses").clear();
    }

    saveLesson(lesson: Lesson): Promise<void> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let transaction = databaseConnector.db.transaction("lessons", "readwrite");

            transaction.onerror = (event) => {
                console.log(event)
            };

            let objectStore = transaction.objectStore("lessons");

            let request = objectStore.add(lesson);
            request.onsuccess = (event) => {
                resolve();
            };
        });
    }

    getLessons(): Promise<Lesson[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let objectStore = databaseConnector.db.transaction("lessons").objectStore("lessons");
            let lessons: Lesson[] = [];

            objectStore.openCursor().onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    lessons.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(lessons)
                }
            };
        });
    }

    getLessonsByCourse(course: Course): Promise<Lesson[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let objectStore = databaseConnector.db.transaction("lessons").objectStore("lessons");
            let lessons: Lesson[] = [];

            let range = IDBKeyRange.only(course.id);

            let index = objectStore.index("courseId");
            index.openCursor(range).onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    cursor.value.course = course;
                    lessons.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(lessons)
                }
            };
        });
    }

    getLessonsByCourseId(courseId: number): Promise<Lesson[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let objectStore = databaseConnector.db.transaction("lessons").objectStore("lessons");
            let lessons: Lesson[] = [];

            let range = IDBKeyRange.only(courseId);

            let index = objectStore.index("courseId");
            index.openCursor(range).onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    lessons.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(lessons)
                }
            };
        });
    }

    clearLessons(): void {
        this.db.transaction("lessons", "readwrite").objectStore("lessons").clear();
    }

    saveAssignments(assignment: Assignment): Promise<void> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let transaction = databaseConnector.db.transaction("saniAssignments", "readwrite");

            transaction.onerror = (event) => {
                console.log(event)
            };

            let objectStore = transaction.objectStore("saniAssignments");

            assignment["dateSec"] = new Date(assignment.date).getTime();
            let request = objectStore.add(assignment);
            request.onsuccess = (event) => {
                resolve();
            };
        });

    }

    updateAssignment(assignment: Assignment): Promise<void> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {
            let isUpdated: boolean = false;
            let transaction = databaseConnector.db.transaction(['saniAssignments'], 'readwrite');
            let objectStore = transaction.objectStore('saniAssignments');
            let vendorIndex = objectStore.index('assignmentId');
            let idbKeyRange = IDBKeyRange.only([assignment.date, assignment.lesson, assignment.type]);
            let cursorRequest = vendorIndex.openCursor(idbKeyRange);
            cursorRequest.onsuccess = e => {
                const cursor = e.target.result;
                if (cursor) {
                    let cursorValue = cursor.value;
                    cursorValue.userId = assignment.userId;
                    cursor.update(cursorValue);
                    isUpdated = true;
                    resolve();
                    cursor.continue();
                }
            }
            if (!isUpdated) {
                assignment["dateSec"] = new Date(assignment.date).getTime();
                let request = objectStore.put(assignment);
                request.onsuccess = () => {
                    resolve();
                };
            }
        });

    }

    getAssignments(): Promise<Assignment[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let objectStore = databaseConnector.db.transaction("saniAssignments").objectStore("saniAssignments");
            let assignments: Assignment[] = [];

            objectStore.openCursor().onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    assignments.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(assignments)
                }
            };
        });
    }

    getAssignemntsByWeek(dateStart): Promise<Assignment[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise(function (resolve, reject) {
            let date = new Date(dateStart);
            let objectStore = databaseConnector.db.transaction("saniAssignments").objectStore("saniAssignments");
            let lessons = [];
            let firstday = date.getTime() - 1;
            let lastday = date.setDate(date.getDate() + 5);
            let range = IDBKeyRange.bound(firstday, lastday, false, false);

            let index = objectStore.index("dateSec");

            index.openCursor(range).onsuccess = function (event) {
                let cursor = event.target.result;
                if (cursor) {
                    lessons.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(lessons)
                }
            };
        });
    }

    getAssignmentsByUserId(userId: number): Promise<Assignment[]> {

        let databaseConnector: DatabaseConnector = this;
        return new Promise(function (resolve, reject) {
            let objectStore = databaseConnector.db.transaction("saniAssignments").objectStore("saniAssignments");
            let range = IDBKeyRange.only(userId);

            let index = objectStore.index("userId");
            let assignments: Assignment[] = [];
            index.openCursor(range).onsuccess = function (event) {
                let cursor = event.target.result;
                if (cursor) {
                    assignments.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(assignments)
                }
            };
        });
    }

    getAssignmentsByDateLesson(date: string, lesson: number): Promise<Assignment[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise(function (resolve, reject) {
            let objectStore = databaseConnector.db.transaction("saniAssignments").objectStore("saniAssignments");
            let range = IDBKeyRange.only([date, lesson]);

            let index = objectStore.index("dateLesson");
            let assignments: Assignment[] = [];
            index.openCursor(range).onsuccess = function (event) {
                let cursor = event.target.result;
                if (cursor) {
                    assignments.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(assignments)
                }
            };
        });
    }

    getAssignmentsByDateLessonType(date: string, lesson: number, type: number): Promise<Assignment> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise(function (resolve, reject) {
            let objectStore = databaseConnector.db.transaction("saniAssignments").objectStore("saniAssignments");
            let range = IDBKeyRange.only([date, lesson, type]);

            let index = objectStore.index("assignmentId");
            index.openCursor(range).onsuccess = function (event) {
                let cursor = event.target.result;
                if (cursor) {
                    resolve(cursor.value);
                    return;
                } else {
                    reject();
                }
            };
        });
    }

    deleteAssignment(date: string, lesson: number, type: number) {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {
            let objectStore = databaseConnector.db.transaction("saniAssignments", "readwrite").objectStore("saniAssignments");
            let range = IDBKeyRange.only([date, lesson, type]);

            let index = objectStore.index("assignmentId");
            let assignments: Assignment[] = [];
            index.openCursor(range).onsuccess = function (event) {
                let cursor = event.target.result;
                if (cursor) {
                    assignments.push(cursor.value);
                    objectStore.delete(cursor.value.key);
                    cursor.continue();
                } else {
                    resolve(assignments)
                }
            };
        });

    }

    clearAssignments(): void {
        this.db.transaction("saniAssignments", "readwrite").objectStore("saniAssignments").clear();
    }

    saveExam(exam: any): Promise<void> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let transaction = databaseConnector.db.transaction("exams", "readwrite");

            transaction.onerror = (event) => {
                console.log(event)
            };

            let objectStore = transaction.objectStore("exams");

            exam["dateSec"] = new Date(exam.date).getTime();
            let request = objectStore.add(exam);
            request.onsuccess = (event) => {
                resolve();
            };
        });

    }

    getExams(): Promise<any[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let objectStore = databaseConnector.db.transaction("exams").objectStore("exams");
            let exams: any[] = [];

            objectStore.openCursor().onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    exams.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(exams)
                }
            };
        });
    }

    getExamsByWeek(dateStart): Promise<Exam[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise(function (resolve, reject) {
            let date = new Date(dateStart);
            let objectStore = databaseConnector.db.transaction("exams").objectStore("exams");
            let exams = [];
            let firstday = date.getTime() - 1;
            let lastday = date.setDate(date.getDate() + 5);
            let range = IDBKeyRange.bound(firstday, lastday, false, false);

            let index = objectStore.index("dateSec");

            index.openCursor(range).onsuccess = function (event) {
                let cursor = event.target.result;
                if (cursor) {
                    exams.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(exams)
                }
            };
        });
    }

    getExamsByCourse(course: Course): Promise<any[]> {
        let databaseConnector: DatabaseConnector = this;
        return new Promise((resolve, reject) => {

            let objectStore = databaseConnector.db.transaction("exams").objectStore("exams");
            let lessons: Lesson[] = [];

            let range = IDBKeyRange.only(course.id);

            let index = objectStore.index("courseId");
            index.openCursor(range).onsuccess = (event) => {
                let cursor = event.target.result;
                if (cursor) {
                    cursor.value.course = course;
                    lessons.push(cursor.value);
                    cursor.continue();
                } else {
                    resolve(lessons)
                }
            };
        });
    }

    clearExams(): void {
        this.db.transaction("exams", "readwrite").objectStore("exams").clear();
    }
}

class User {
    id: number;
    firstname: string;
    lastname: string;
    username: string;
    displayname: string;
    grade: string;
    assignedLessons: number;
    courseIds: number[];
    examCourseIds: number[];
    timeTable: {};
    dn: string;
}

class Course {
    grade: string;
    subject: string;
    group: string;
    id: number;
}

class Assignment {
    id: number;
    userId: number;
    date: string;
    lesson: number;
    type: number;
}

class Lesson {
    courseId: number;
    course: Course;
    lesson: number;
    weekday: number;
    id: number;
}

class Exam {
    date: string;
    from: string;
    to: string;
    courseId: number
}